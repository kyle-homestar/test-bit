<?php
/*
Plugin Name:          Test Bitbucket Plugin
Plugin URI:           https://bitbucket.org/afragen/test-bitbucket-plugin
Description:          This plugin is used for testing functionality of Bitbucket updating of plugins.
Version:              0.3.3
Author:               Andy Fragen
License:              GNU General Public License v2
License URI:          http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Bitbucket Plugin URI: https://bitbucket.org/afragen/test-bitbucket-plugin
Text Domain:          test-bitbucket-plugin
Domain Path:          /languages
Requires WP:          4.0
Requires PHP:         5.3.0
*/

